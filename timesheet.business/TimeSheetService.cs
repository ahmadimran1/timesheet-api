﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class TimeSheetService
    {

        public TimesheetDb db { get; }
        public TimeSheetService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }
        /// <summary>
        /// Save Time Log
        /// </summary>
        /// <param name="timeLog"></param>
        public void SaveTimeLog(TimeLog timeLog)
        {
            timeLog.CreationTime = DateTime.Now;
            db.TimeLogs.Add(timeLog);
            db.SaveChanges();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeId"></param>
        public List<TimeLog> GetTimeLog(int employeeId)
        {
            return this.db.TimeLogs.ToList().Where(x=>x.EmployeeId==employeeId).ToList();
        }
    }
}
