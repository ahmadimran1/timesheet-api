﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class EmployeeVM
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int WeeklyTotal { get; set; }
        public Double WeeklyEffortAverage { get; set; }
    }
}
