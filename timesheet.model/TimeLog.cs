﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class TimeLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string Day { get; set; }
        public int Hours { get; set; }


        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }


        [ForeignKey("Task")]
        public int TaskId { get; set; }
        public Task Task { get; set; }

        public DateTime CreationTime { get; set; }

    }
}