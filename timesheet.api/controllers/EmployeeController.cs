﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService employeeService;
        private readonly TimeSheetService timeSheetService;
        public EmployeeController(EmployeeService employeeService, TimeSheetService timeSheetService)
        {
            this.employeeService = employeeService;
            this.timeSheetService = timeSheetService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var employees = this.employeeService.GetEmployees().ToList();

            List<EmployeeVM> employeeVM = new List<EmployeeVM>();
            foreach (var emp in employees)
            {
                var logs = this.timeSheetService.GetTimeLog(emp.Id);
                var sumOfHours = logs.Sum(item => item.Hours);
                employeeVM.Add(new EmployeeVM()
                {
                    Id=emp.Id,
                    Code = emp.Code,
                    Name = emp.Name,
                    WeeklyTotal = sumOfHours,
                    WeeklyEffortAverage = sumOfHours / 7
                });
            }

            return new ObjectResult(employeeVM);
        }
    }
}