﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService taskService;
        public TaskController(TaskService taskService)
        {
            this.taskService = taskService;
           
        }
        /// <summary>
        /// Get all tasks
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var items = this.taskService.GetTasks();
            return new ObjectResult(items);
        }
       
    }
}