﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/timesheet")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly TaskService taskService;
        private readonly TimeSheetService timeSheetService;
        public TimesheetController(TaskService taskService, TimeSheetService timeSheetService)
        {
            this.taskService = taskService;
            this.timeSheetService = timeSheetService;
        }
        /// <summary>
        /// Get all time log data
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet("getall/{employeeId}")]
        public IActionResult GetAll(int employeeId)
        {
            var items = this.timeSheetService.GetTimeLog(employeeId);
            return new ObjectResult(items);
        }
        /// <summary>
        /// Save log time on task
        /// </summary>
        /// <param name="timeLog"></param>
        /// <returns></returns>
        [HttpPost("saveTimeLog")]
        public IActionResult SaveTimeLog([FromBody] List<TimeLog> timeLog)
        {
            try
            {
                timeLog.ForEach(x => x.Id = 0);
                timeLog.ForEach(x=> this.timeSheetService.SaveTimeLog(x));
                return new ObjectResult(new { Status = true });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}